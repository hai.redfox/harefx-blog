require "rails_helper"

module Mutations
  module Users
    RSpec.describe Update, type: :request do
      describe ".resolve" do
        let(:user) { FactoryBot.create :user }

        it "update with new inputs" do
          current_user_id = user.id
          post "/graphql", params: { query: query(current_user_id) }

          expect(User.find(current_user_id)).to have_attributes(first_name: "Henry",
                                                                last_name: "Cup",
                                                                address: "Big Apple",
                                                                date_of_birth: Date.parse("2002-01-01"),
                                                                favorite_quote: "1\% better")
        end

        def query(id)
          <<~GQL
            mutation {
              updateUser(input: {
                id: #{id},
                updateInfo: {
                  firstName: "Henry",
                  lastName: "Cup",
                  address: "Big Apple",
                  dateOfBirth: "2002-01-01",
                  favoriteQuote: "1% better"
                }
              }) {
                user {
                  firstName
                  lastName
                }
              }
            }
          GQL
        end
      end
    end
  end
end