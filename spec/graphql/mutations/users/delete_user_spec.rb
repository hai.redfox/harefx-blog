require "rails_helper"

module Mutations
  module Users
    RSpec.describe Delete, type: :request do
      describe ".resolve" do
        let(:user) { FactoryBot.create :user }

        it "delete the existing user" do
          test_user = user
          post "/graphql", params: { query: query(test_user.id) }
          expect(User.kept.count).to eq(0)
        end

        def query(id)
          <<~GQL
            mutation {
              deleteUser(input: {
                id: #{id}
              }) {
                clientMutationId
              }
            }
          GQL
        end
      end
    end
  end
end