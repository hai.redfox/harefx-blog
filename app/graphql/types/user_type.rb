module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :email, String, null: false
    field :first_name, String, null: true
    field :last_name, String, null: true
    field :address, String, null: true
    field :date_of_birth, Types::DateType, null: true
  end
end