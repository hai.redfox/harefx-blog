module Types
  class UpdateUserDataInput < BaseInputObject
    graphql_name "UPDATE_USER_DATA"

    argument :first_name, String, required: false
    argument :last_name, String, required: false
    argument :address, String, required: false
    argument :date_of_birth, String, required: false
    argument :favorite_quote, String, required: false
  end
end