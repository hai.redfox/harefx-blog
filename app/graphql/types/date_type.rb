module Types
  class DateType < Types::BaseScalar
    def self.coerce_input(value, _context)
      Time.zone.parse(value)
    end
  end
end