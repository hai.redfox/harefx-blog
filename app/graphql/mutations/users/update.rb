module Mutations
  module Users
    class Update < BaseMutation
      argument :id, ID, required: true
      argument :update_info, Types::UpdateUserDataInput, required: false

      field :user, Types::UserType, null: false

      def resolve(id:, **args)
        user = User.find(id)
        user.update!(args.transform_values(&:to_h)[:update_info])
        { user: user }
      end
    end
  end
end