module Mutations
  module Users
    class Delete < BaseMutation
      argument :id, ID, required: true
      field :success, Boolean, null: false

      def resolve(**args)
        user = User.find(args[:id])
        unless user.discarded?
          user.discard
          return { success: true }
        end
        { success: false }
      end
    end
  end
end